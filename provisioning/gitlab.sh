#!/bin/bash

set -o errexit

# URL = gitlab.devcircus.com
SERVER_SUBDOMAIN="gitlab"
SERVER_DOMAIN="devcircus.com"

function installSoftwareBase {
  # Software base
  sudo DEBIAN_FRONTEND=noninteractive apt-get install -y \
    curl \
    openssh-server \
    ca-certificates
  # Postfix
  sudo debconf-set-selections <<< "postfix postfix/mailname string $SERVER_SUBDOMAIN.$SERVER_DOMAIN"
  sudo debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
  sudo apt-get install -y postfix
}

function installGitlab {
  curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash
  sudo apt-get install -y gitlab-ce
}

function configureSSL {
	# Import the certificates
	pushd /etc/ssl/private
		cp /vagrant/config/certs/* .
		chmod 400 $SERVER_DOMAIN-keypair.pem
		cp $SERVER_DOMAIN-crt.pem /usr/local/share/ca-certificates/$SERVER_SUBDOMAIN.$SERVER_DOMAIN.crt
		update-ca-certificates --verbose
	popd
	# Link the certs
	install -m 700 -o root -g root -d /etc/gitlab/ssl
	ln -s /etc/ssl/private/$SERVER_DOMAIN-keypair.pem /etc/gitlab/ssl/$SERVER_SUBDOMAIN.$SERVER_DOMAIN.key
	ln -s /etc/ssl/private/$SERVER_DOMAIN-crt.pem /etc/gitlab/ssl/$SERVER_SUBDOMAIN.$SERVER_DOMAIN.crt
	# Configure Gitlab
	sed -i -E "s,^(external_url\s+).+,\1'https://$SERVER_SUBDOMAIN.$SERVER_DOMAIN'," /etc/gitlab/gitlab.rb
	sed -i -E "s,^(\s*#\s*)?(nginx\['redirect_http_to_https'\]\s+).+,\2= true," /etc/gitlab/gitlab.rb
}

function showChangesInGitlaConfigurationFile {
	# show the changes we've made to gitlab.rb.
	diff -u /opt/gitlab/etc/gitlab.rb.template /etc/gitlab/gitlab.rb || test $? = 1
}

#function disableSignup {
#	# Disable user signup.
#	sed -i -E "s,^(\s*#\s*)?(nginx\['gitlab_signup_enabled'\]\s+).+,\2= false," /etc/gitlab/gitlab.rb
#}

function reconfigureGitlab {
	# Reconfigure the Gitlab server
	sudo gitlab-ctl reconfigure
}

function showServicesStatus {
	# See the gitlab services status.
	sudo gitlab-ctl status
}

function showGitlabEnvironmentInfo {
	# Show the gitlab environment info.
	sudo gitlab-rake gitlab:env:info
}

function showSoftwareVersions {
	# Show software versions.
	dpkg-query --showformat '${Package} ${Version}\n' --show gitlab-ce
	/opt/gitlab/embedded/bin/git --version
	/opt/gitlab/embedded/bin/ruby -v
	gitlab-rails --version
	gitlab-psql --version
	/opt/gitlab/embedded/bin/redis-server --version
	/opt/gitlab/embedded/sbin/nginx -v
}

function showFinalBanner {
	# show GitLab address and root credentials.
	echo "GitLab is running at https://$SERVER_SUBDOMAIN.$SERVER_DOMAIN"
	echo 'Sign in with the root user and the password password'
}

echo "Setup Gitlab"

installSoftwareBase
installGitlab
configureSSL
#disableSignup
showChangesInGitlaConfigurationFile
reconfigureGitlab
showServicesStatus
showGitlabEnvironmentInfo
showSoftwareVersions
showFinalBanner

echo "Gitlab setup complete"
